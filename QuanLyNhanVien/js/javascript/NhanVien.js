    /**
    * Muc tieu:
        *  - Tao doi tuong nhan vien
    */

    function NhanVien (_taiKhoan, _hoTen, _email, _password, _date, _luongCoBan, _chucVu, _gioLamTrongThang) 
    {
        this.TaiKhoan = _taiKhoan,
        this.HoTen = _hoTen,
        this.Email = _email,
        this.PassWord = _password,
        this.Date = _date,
        this.LuongCoBan = _luongCoBan,
        this.ChucVu = _chucVu,
        this.GioLamTrongThang = _gioLamTrongThang,
        this.TinhLuong = this.TinhTongLuong(),
        this.XepLoaiNhanVien = this.XepLoai()
    }

    // add method result Luong
    NhanVien.prototype.TinhTongLuong = function ()
    {
        if(this.ChucVu == 'Sếp') {
            return this.LuongCoBan*3
        } else if (this.ChucVu == 'Trưởng Phòng') {
            return  this.LuongCoBan*2
        } else {
            return this.LuongCoBan
        }
    }

    // add method rank
    NhanVien.prototype.XepLoai = function () {
        let ketQua = ''
        if (this.GioLamTrongThang >=192) {
            ketQua = 'Xuất Sắc'
        } else if (this.GioLamTrongThang >=176) {
            ketQua = 'Giỏi'
        } else if (this.GioLamTrongThang >=160) {
            ketQua = 'Khá'
        } else {
            ketQua= 'Trung Bình'
        }
        return ketQua;
}
