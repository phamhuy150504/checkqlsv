    function Validation() 
    {
        this.KiemTraRong = function (value) {
            if(value.trim() === '')
            {
            return true;
            } 
            return false;
        };

        this.KiemTraDoDai = function (value, minLength, maxLength) {
            if(value.length < minLength || value.length > maxLength) 
            {
                return true
            }
            return false
        };

        this.KiemTraNum = function (value) {
            let reg = /^([0-9]+-)*([0-9]+)$/
            if (!reg.test(value)) 
            {
                return true
            }
            return false
        };

        this.KiemTraString = function (mystring) {
            let rreg = /[A-Z]/g
            if (!mystring.match(rreg)) 
            {
                return true
            }
            return false
        };

        this.KiemTraEmail = function (myEmail) 
        {
            const re =
            /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            if(!myEmail.match(re)) 
            {
                return true
            } 
            return false
        };

        this.KiemTraMatKhau = function (myPassWord) 
        {
            let reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{6,30}$/
            if(!myPassWord.match(reg)) 
            {
                return true
            } 
            return false
        };

        this.KiemTraLuong = function (soLuong) {
            if (soLuong >= 1000000 && soLuong <= 20000000) 
            {
                return false
            }
            return true
        };

        this.KiemTraGioLam = function (soGioLam) {
            if(soGioLam >= 80 && soGioLam <=200) 
            {
                return false
            }
            return true
        };

        this.KiemTraTrungTaiKhoan = function (DSNV, id) {
            for (var i = 0; i < DSNV.length; i++) {
                if(DSNV[i].TaiKhoan === id) 
                {
                    return true
                }
                return false
            }
        }
    }