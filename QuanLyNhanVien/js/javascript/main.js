    // Array chúa nhân viên
    let DSSV = [];

    // gọi đội tượng validate
    let Validate = new Validation();

    //Chuc Nang cua Form
    let ChucNang = new ListChucNang();

    // Dom id element
    function Dom(GetEle) 
    {
    let dom = document.getElementById(GetEle)
    return dom;
    }   

    /**
    * Mục Tiêu: 
    * 1. Lấy thông tin từ Json lên bằng localStorage.getItem('<Ten-luu>')
    * 2. Kiểm tra nếu giá trị != null  ==>  Json.parse ra và gán cho DSSV 
    * 3. Render DSSV vua gan
    */
    let jsonDanhSachNhanVien = localStorage.getItem('DanhSachNV')
    if (jsonDanhSachNhanVien != null) 
    {
            DSSV= JSON.parse(jsonDanhSachNhanVien)

            CapNhapNhanVien(DSSV)
    }
  
    // function thong bao loi
    function thongBaoLoi(id, indexErr) 
    {
        return Dom(id).innerHTML = indexErr
    }

    // cac cau error 
    let arrErr = ["Bạn chưa nhập thông tin", 
                  'Tài khoản từ 4-6 ký tự', 
                  'Tên của bạn phải là chữ', 
                  'Email không hợp lệ',
                  'Mật khẩu từ 6-10 ký tự, có chữ in hoa, số, ký tự đặc biệt', 
                  'Lương từ 1.000.000 - 20.000.000', 
                  'Số giờ làm từ 80-200 giờ',
                  'Tài khoản phải là ký số', 
                  'Tài khoản đã tồn tại']

    // --------Function Them Nhan Vien --------------
    function LayThongTinTuForm() 
    {
        // Lay thong tin nguoi dung dien vao
        let taiKhoan = Dom('tknv').value
        let hoTen = Dom('name').value
        let email = Dom('email').value
        let matKhau = Dom('password').value
        let date = Dom('datepicker').value
        let luongCoban = Dom('luongCB').value
        let chucVu = Dom('chucvu').value
        let gioLamTrongThang = Dom('gioLam').value

        let test = 0;
        // kiem tra tai khoan
        if(KiemTraRong('tknv', taiKhoan))  {
            thongBaoLoi('tbTKNV', arrErr[0])
            test++
        } else if (Validate.KiemTraNum(taiKhoan)) {
            thongBaoLoi('tbTKNV', arrErr[7])
            test++
        } else if (Validate.KiemTraDoDai(taiKhoan, 4, 6)) {
            test++
            thongBaoLoi('tbTKNV', arrErr[1])
        } else if (Validate.KiemTraTrungTaiKhoan(DSSV, taiKhoan)) {
            thongBaoLoi('tbTKNV', arrErr[8])
            test++
        } else {
            Dom('tknv').style.borderColor = 'green'
            thongBaoLoi('tbTKNV', '')
        }

        // kiem tra ten
        if(KiemTraRong('name', hoTen))  {
            thongBaoLoi('tbTen', arrErr[0])
            test++
        } else if (Validate.KiemTraString(hoTen)) {
            test++
            thongBaoLoi('tbTen', arrErr[2])
        } else {
            Dom('name').style.borderColor = 'green'
            thongBaoLoi('tbTen', '')
        }

        // Kiem tra email
        if(KiemTraRong('email', email))  {
            thongBaoLoi('tbEmail', arrErr[0])
            test++
        } else if (Validate.KiemTraEmail(email)) {
            thongBaoLoi('tbEmail', arrErr[3])
            test++
        }else {
            Dom('email').style.borderColor = 'green'
            thongBaoLoi('tbEmail', '')
        }

        // kiem tra mat khau
        if(KiemTraRong('password', matKhau))  {
            thongBaoLoi('tbMatKhau', arrErr[0])
            test++
        } else if (Validate.KiemTraMatKhau(matKhau)) {
            thongBaoLoi('tbMatKhau', arrErr[4])
            test++
        }
        else {
            Dom('password').style.borderColor = 'green'
            thongBaoLoi('tbMatKhau', '')
        }


        // kiem tra date
        if(KiemTraRong('datepicker', date)) {
            thongBaoLoi('tbNgay', arrErr[0])
            test++
        } else {
            Dom('datepicker').style.borderColor = 'green'
            thongBaoLoi('tbNgay', '')
        }


        // Kiem Tra Luong
        if(KiemTraRong('luongCB', luongCoban))  {
            thongBaoLoi('tbLuongCB', arrErr[0])
            test++
        } else if (Validate.KiemTraLuong(luongCoban*1)) {
            thongBaoLoi('tbLuongCB', arrErr[5])
            test++
        } else {
            Dom('luongCB').style.borderColor = 'green'
            thongBaoLoi('tbLuongCB', '')
        }

        // Kiem Tra Gio Lam
        if(KiemTraRong('gioLam', gioLamTrongThang))  {  
            thongBaoLoi('tbGiolam', arrErr[0])
            test++
        } else if (Validate.KiemTraGioLam(gioLamTrongThang)) {
            thongBaoLoi('tbGiolam', arrErr[6])
            test++
        }
        else {
            Dom('gioLam').style.borderColor = 'green'
            thongBaoLoi('tbGiolam', '')
        }

        // Kiem tra Chuc vu
        if (chucVu == 0) {
            Dom('chucvu').style.borderColor = 'red'
            thongBaoLoi('tbChucVu', arrErr[0])
            test++
        }
        else {
            Dom('chucvu').style.borderColor = 'green'
            thongBaoLoi('tbChucVu', '')
        }
    
        // Add Nhan vien vao mang
        if (test != 0) {
            return 
        } 


        // gán value cho nhân viên sau khi kiểm tra dữ liệu nhập vào là true;
        let nhanVien = new NhanVien(taiKhoan, hoTen, email, matKhau, date, luongCoban, chucVu, gioLamTrongThang)

        // push cho mảng chứa nhân viên
        DSSV.push(nhanVien)

        // Them Nhan Vien
        CapNhapNhanVien(DSSV)    

        // Đẩy thông thin lên Json
        SetJson(DSSV)
    
        // gọi hàm reload browser 
        refreshPage()   

        return nhanVien
}


    // --------- Set Json --------
    function SetJson (DSNV) 
    {
        let JsonDSSV = JSON.stringify(DSNV)
        localStorage.setItem('DanhSachNV', JsonDSSV)
    }
 

    // ------ reload browser sau khi click button --------
    function refreshPage()
    {
    window.location.reload();
    } 


    // ---- validate Rong -------
    function KiemTraRong (id, value) 
    {
        if(Validate.KiemTraRong(value)) {
            Dom(id).style.borderColor = 'red'
            return true;
        }
        return false
    }


    // ------------- Thêm nhân viên --------------
    function CapNhapNhanVien(arrDSSV) 
    {
        let contentHTML = '';
        let arr = arrDSSV.length
        for (var i = 0; i < arr ; i++) {
            contentHTML += `
            <tr>
                <td>${arrDSSV[i].TaiKhoan}</td>
                <td>${arrDSSV[i].HoTen}</td>
                <td>${arrDSSV[i].Email}</td>
                <td>${arrDSSV[i].Date}</td>
                <td>${arrDSSV[i].ChucVu}</td>
                <td>${arrDSSV[i].TinhLuong}</td>
                <td>${arrDSSV[i].XepLoaiNhanVien}</td>
                <td>
                <button onclick="XoaNhanVien(${arrDSSV[i].TaiKhoan})" class="btn btn-danger">Xóa</button>
                <button data-toggle="modal" data-target="#myModal" onclick="SuaNhanVien(${arrDSSV[i].TaiKhoan})" class="btn btn-warning">Sửa</button>
                </td>
            </tr>
            `
        }
        return Dom('tableDanhSach').innerHTML = contentHTML
    }


    // ----- function Xóa Nhân Viên -------
    function XoaNhanVien(id) {
        let ViTri = ChucNang.XoaNhanVien(id, DSSV)
        console.log(ViTri)
        if (ViTri != -1) {
            DSSV.splice(ViTri,1)
            CapNhapNhanVien(DSSV)
            SetJson(DSSV)
        }
    }

    // -------- function Lấy thông tin từ table lên form ---------
    function SuaNhanVien(id) {
        for (var i = 0; i < DSSV.length; i++) {
            if (DSSV[i].TaiKhoan*1 === id) {
                Dom('tknv').value =  DSSV[i].TaiKhoan
                Dom('tknv').disabled = true
                Dom('name').value =  DSSV[i].HoTen
                Dom('email').value =  DSSV[i].Email
                Dom('password').value =  DSSV[i].PassWord
                Dom('datepicker').value =  DSSV[i].Date
                Dom('luongCB').value =  DSSV[i].LuongCoBan
                Dom('chucvu').value =  DSSV[i].ChucVu
                Dom('gioLam').value =  DSSV[i].GioLamTrongThang
            }

        }
    }


    //--------- function TinhLuongCapNhap ---------
    function tinhLuong(id, luongCB) {
        let tong = 0
        if(id == 'Sếp') {
            tong = luongCB *3
        } else if (id == 'Trưởng Phòng') {
            tong = luongCB *2
        } else {
            tong = luongCB*1
        }
        return tong
    }



    //------- function XepLoaiCapnhap --------
    function xepLoai(soGioLam) {
        let ketQua = ''
        if (soGioLam >=192) {
            ketQua = 'Xuất Sắc'
        } else if (soGioLam >=176) {
            ketQua = 'Giỏi'
        } else if (soGioLam >=160) {
            ketQua = 'Khá'
        } else {
            ketQua= 'Trung Bình'
        }
        return ketQua;
    }


    // Hàm cập nhập nhân viên
    function CapNhapNv() {  
        let test = 0;
        let taiKhoan = Dom('tknv').value*1
        let viTri = ChucNang.CapNhapSinhVien(taiKhoan, DSSV)
        
        if(viTri != -1) {
            DSSV[viTri].HoTen = Dom('name').value
            DSSV[viTri].Email = Dom('email').value
            DSSV[viTri].PassWord = Dom('password').value
            DSSV[viTri].Date = Dom('datepicker').value
            DSSV[viTri].LuongCoBan = Dom('luongCB').value
            DSSV[viTri].ChucVu = Dom('chucvu').value
            DSSV[viTri].GioLamTrongThang = Dom('gioLam').value
            DSSV[viTri].TinhLuong = tinhLuong(DSSV[viTri].ChucVu, DSSV[viTri].LuongCoBan)
            DSSV[viTri].XepLoaiNhanVien = xepLoai(DSSV[viTri].GioLamTrongThang)
        }   
        
        // kiem tra ten
        if(KiemTraRong('name', DSSV[viTri].HoTen))  {
            thongBaoLoi('tbTen', arrErr[0])
            test++
        } else if (Validate.KiemTraString(DSSV[viTri].HoTen)) {
            test++
            thongBaoLoi('tbTen', arrErr[2])
        } else {
            Dom('name').style.borderColor = 'green'
            thongBaoLoi('tbTen', '')
        }

        // Kiem tra email
        if(KiemTraRong('email', DSSV[viTri].Email))  {
            thongBaoLoi('tbEmail', arrErr[0])
            test++
        } else if (Validate.KiemTraEmail(DSSV[viTri].Email)) {
            thongBaoLoi('tbEmail', arrErr[3])
            test++
        }else {
            Dom('email').style.borderColor = 'green'
            thongBaoLoi('tbEmail', '')
        }

        // kiem tra mat khau
        if(KiemTraRong('password', DSSV[viTri].PassWord))  {
            thongBaoLoi('tbMatKhau', arrErr[0])
            test++
        } else if (Validate.KiemTraMatKhau(DSSV[viTri].PassWord)) {
            thongBaoLoi('tbMatKhau', arrErr[4])
            test++
        }
        else {
            Dom('password').style.borderColor = 'green'
            thongBaoLoi('tbMatKhau', '')
        }

        // kiem tra date
        if(KiemTraRong('datepicker', DSSV[viTri].Date)) {
            thongBaoLoi('tbNgay', arrErr[0])
            test++
        } else {
            Dom('datepicker').style.borderColor = 'green'
            thongBaoLoi('tbNgay', '')
        }

        // Kiem Tra Luong
        if(KiemTraRong('luongCB', DSSV[viTri].LuongCoBan))  {
            thongBaoLoi('tbLuongCB', arrErr[0])
            test++
        } else if (Validate.KiemTraLuong(DSSV[viTri].LuongCoBan*1)) {
            thongBaoLoi('tbLuongCB', arrErr[5])
            test++
        } else {
            Dom('luongCB').style.borderColor = 'green'
            thongBaoLoi('tbLuongCB', '')
        }

        // Kiem Tra Gio Lam
        if(KiemTraRong('gioLam', DSSV[viTri].GioLamTrongThang))  {  
            thongBaoLoi('tbGiolam', arrErr[0])
            test++
        } else if (Validate.KiemTraGioLam(DSSV[viTri].GioLamTrongThang)) {
            thongBaoLoi('tbGiolam', arrErr[6])
            test++
        }
        else {
            Dom('gioLam').style.borderColor = 'green'
            thongBaoLoi('tbGiolam', '')
        }

        // Kiem tra Chuc vu
        if (DSSV[viTri].ChucVu == 0) {
            Dom('chucvu').style.borderColor = 'red'
            thongBaoLoi('tbChucVu', arrErr[0])
            test++
        }
        else {
            Dom('chucvu').style.borderColor = 'green'
            thongBaoLoi('tbChucVu', '')
        }
    
        // Add Nhan vien vao mang
        if (test != 0) {
            return 
        } 
        
        //RENDER nhan Vien
        CapNhapNhanVien(DSSV)

        // Đẩy thông thin lên Json
        SetJson(DSSV)
      
        // gọi hàm reload browser 
        refreshPage() 
    }

    // ------- function TimKiemNhanVien -------
    function TimKiemNhanVien() {
        //Lấy oninput user
        let tuKhoa = Dom('searchName').value;

        //mảng chứa từ khóa
        let arrSvSearch = []

        DSSV.forEach(function (sinhVien) {
            if(sinhVien.XepLoaiNhanVien.toLowerCase().trim().search(tuKhoa.toLowerCase().trim()) != -1) 
            {
            arrSvSearch.push(sinhVien)
            }
    })  
        CapNhapNhanVien(arrSvSearch)
    }


